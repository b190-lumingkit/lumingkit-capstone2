const dotenv = require('dotenv');
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Import routes
// user-related routes
const userRoutes = require("./routes/user/userRoutes")
const addressRoutes = require("./routes/user/addressRoutes")
const adminRoutes = require("./routes/user/adminRoutes")
// product-related routes
const productRoutes = require("./routes/product/productRoutes")
const reviewRoutes = require("./routes/product/reviewRoutes")
const orderRoutes = require("./routes/product/orderRoutes")
const ingredRoutes = require("./routes/product/ingredRoutes")
const cartRoutes = require("./routes/product/cartRoutes");

const app = express();
dotenv.config()

// MongoDB Connection
mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true });
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the database.`));


app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// ROUTES
app.use("/admin-panel", adminRoutes);
app.use("/users", userRoutes);
app.use("/users/addresses", addressRoutes);
app.use("/users/orders", orderRoutes);

app.use("/products", productRoutes);
app.use("/products/reviews", reviewRoutes);
app.use("/products/ingredients", ingredRoutes);

app.use("/cart", cartRoutes);

app.listen(process.env.PORT || 4000, () => 
{ 
    console.log(`API now online at port ${process.env.PORT || 4000}`);
});