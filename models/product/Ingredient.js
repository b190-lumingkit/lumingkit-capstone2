const mongoose = require("mongoose");

const ingredSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Ingredient name is required"]
    },
    category: {
        type: String,
        required: [true, "Category is required"]
    },
    stocks: {
        type: Number,
        required: [true, "Stocks is required"]
    },
    pricePerServing: {
        type: Number,
        required: [true, "Price per serving is required"]
    },
    addedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Ingredient", ingredSchema);