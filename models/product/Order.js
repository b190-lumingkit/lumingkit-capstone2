const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "userId name is required"]
    },
    products: {
        type: mongoose.SchemaTypes.Mixed,
        required: [true, "Products are required"]
    },
    totalPrice: {
        type: Number,
        required: [true, "Total price is required"]
    },
    address: {
        type: mongoose.SchemaTypes.Mixed,
        required: [true, "Address is required"]
    },
    status: {
        type: String,
        default: "pending"
    },
    paymentMethod: {
        type: String,
        required: [true, "Payment method is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);