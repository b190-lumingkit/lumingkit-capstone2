const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User id is required"]
    },
    products: {
        type: mongoose.SchemaTypes.Mixed,
        required: [true, "Products are required"]
    },
    addedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Cart", cartSchema);