const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "userId is required"]
    },
    productId: {
        type: String,
        required: [true, "userId is required"]
    },
    rating: {
        type: Number,
        min: 0,
        max: 5,
        default: 0
    },
    description: {
        type: String
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Review', reviewSchema);