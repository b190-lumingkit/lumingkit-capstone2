const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    ingredients: [
        {
            name: {
                type: String,
                required: [true, "Ingredient name is required"]
            },
            serving: {
                type: Number,
                default: 1
            },
            pricePerServing: {
                type: Number,
                required: [true, "Price per serving is required"]
            }
        }
    ],
    size: {
        type: String,
        default: "Sakto"
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    rating: {
        type: Number,
        default: 0
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Product', productSchema);