const mongoose = require("mongoose");

const addressSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "userId is required"]
    },
    name: {
        type: String,
        default: "Home"
    },
    line_1: {
        type: String,
        required: [true, "Contact number is required"]
    },
    line_2: {
        type: String
    },
    line_3: {
        type: String,
        required: [true, "Street is required"]
    },
    city: {
        type: String,
        required: [true, "City is required"]
    },
    province: {
        type: String,
        required: [true, "Province is required"]
    },
    addedOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Address", addressSchema);