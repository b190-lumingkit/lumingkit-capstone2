const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"],

    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/, "Enter a valid email."]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    address: [
        {
            addressId: {
                type: String,
            },
            addedOn: {
                type: Date,
                default: new Date()
            },
            isActive: {
                type: Boolean,
                default: true
            }
        }
    ],
    registeredOn: {
        type: Date,
        default: new Date()
    }
});



module.exports = mongoose.model("User", userSchema);