const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const cartController = require("../../controllers/product/cartController");


// get all cart products
router.post("/products", (req, res) =>
{
    cartController.getCartProducts(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// get cart of user
router.get("/", (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    cartController.getUserCart(userId)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// add to cart
router.post("/add", (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    cartController.addToCart(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// update cart
router.put("/update", (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    cartController.updateCart(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

module.exports = router;