const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const reviewController = require("../../controllers/product/reviewController");

// Add product review
router.post("/:productId", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    reviewController.addReview(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

router.get("/:productId", (req, res) =>
{
    reviewController.getProductReviews(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

module.exports = router;