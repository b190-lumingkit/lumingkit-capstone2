const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const orderController = require("../../controllers/product/orderController");

router.post("/checkout", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    orderController.createOrder(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

//get user orders
router.get("/my-orders", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    orderController.getUserOrders(userId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

router.get("/", auth.isAdmin, (req, res) =>
{
    orderController.getAllOrders()
    .then(result => res.send(result))
    .catch(error => res.send(error))
});

router.get("/:orderId", (req, res) => {
    orderController.getOrder(req.params.orderId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// mark order as complete
router.put("/:orderId/:status", auth.isAdmin, (req, res) =>
{
    orderController.completeOrder(req.params.orderId, req.params.status)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// mark order as cancelled
router.put("/:orderId/cancel", auth.isAdmin, (req, res) =>
{
    orderController.cancelOrder(req.params.orderId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})





module.exports = router;