const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const productController = require("../../controllers/product/productController");


router.post("/", auth.verify, (req, res) =>
{
    productController.createProduct(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

//brew your own route
router.post("/brew-your-coffee", auth.verify, (req, res) =>
{
    productController.brewYourCoffee(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// get all products
router.get("/", (req, res) =>
{
    productController.getAllProducts(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// get all products
router.get("/available", (req, res) =>
{
    productController.getAvailableProducts(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// get specific product
router.get("/:productId", (req, res) =>
{
    productController.getProduct(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

router.put("/:productId", auth.isAdmin, (req, res) =>
{
    productController.updateProduct(req.params.productId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

router.put("/:productId/archive", auth.isAdmin, (req, res) =>
{
    productController.removeProduct(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

router.put("/:productId/:status", auth.isAdmin, (req, res) =>
{
    productController.isProductAvailable(req.params.productId, req.params.status)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})




module.exports = router;