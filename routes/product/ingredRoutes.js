const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const ingredController = require("../../controllers/product/ingredController");


router.get("/available", (req, res) =>
{
    ingredController.getAllIngredients()
    .then(result => res.send(result))
    .catch(error => res.send(error));
});

router.post("/", auth.isAdmin, (req, res) =>
{
    ingredController.addIngredient(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
});



router.put("/:ingredId", auth.isAdmin, (req, res) =>
{
    ingredController.updateIngredient(ingredId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
});

module.exports = router;