const express = require("express");
const router = express.Router();
const auth = require("../../auth");
const addressController = require("../../controllers/user/addressController");

// get all addresses
router.get("/", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    addressController.getAllAddress(userId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// add another address
router.post("/", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    addressController.addAddress(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// update specific address
router.put("/:addressId", auth.verify, (req, res) =>
{
    addressController.updateAddress(req.params.addressId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// remove address
router.put("/:addressId/remove", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    addressController.removeAddress(req.params.addressId, userId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

module.exports = router;