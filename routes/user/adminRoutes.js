const express = require("express");
const router = express.Router();
const auth = require("../../auth");

const adminController = require("../../controllers/user/adminController");

router.get("/all-users", auth.isAdmin, (req, res) =>
{
    adminController.getAllUsers()
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

router.put("/promote-user/:userId/:status", auth.isAdmin, (req, res) =>
{
    adminController.promoteUser(req.params.userId, req.params.status)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

router.put("/demote-user/:userId", auth.isAdmin, (req, res) =>
{
    adminController.demoteUser(req.params.userId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

module.exports = router;