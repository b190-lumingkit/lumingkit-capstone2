const express = require("express");
const router = express.Router();
const auth = require("../../auth");

const userController = require("../../controllers/user/userController");

router.post("/register", (req, res) =>
{
    userController.register(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

router.post("/login", (req, res) =>
{
    userController.login(req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// get user profile
router.get("/profile", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    userController.getProfile(userId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// update user profile
router.put("/profile", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    userController.updateProfile(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// update user profile
router.put("/change-password", auth.verify, (req, res) =>
{
    const userId = auth.decode(req.headers.authorization).id;
    userController.changePassword(userId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

module.exports = router;