const User = require("../../models/user/User");
const Address = require("../../models/user/Address");

module.exports.getAllAddress = (userId) =>
{
    return User.findById(userId).then(user =>
    {
        // get all addressId from user.address
        const address_ids = user.address.map(address => (address.isActive === true) ? address.addressId : null)
        // match the address_ids in Address collection and return matching result
        return Address.find({"_id": {"$in": address_ids}}).then(result => result);
    })
}

module.exports.addAddress = (userId, address) =>
{
    const newAddress = new Address(address);
    newAddress.userId = userId;
    return User.findById(userId).then(user =>
    {
        if (user === null) 
        {
            return false
        } 
        else 
        {
            newAddress.save().then(result => result);
            //console.log(newAddress);
            user.address.push({addressId: newAddress._id});
            return user.save().then(user => 
            {
                return true;
            });
        }
    })
}

module.exports.updateAddress = (addressId, address) =>
{
    return Address.findByIdAndUpdate(addressId, address).then(result => {
        if (result) {
            return true;
        } 
        else {
            return false;
        }
    });
}

module.exports.removeAddress = (addressId, userId) =>
{
    return User.findById(userId).then(user =>
    {
        let toArchive = user.address.find(address => address.addressId === addressId);
        toArchive.isActive = false;
        return user.save().then(user => `Address removed.`);
    })
}