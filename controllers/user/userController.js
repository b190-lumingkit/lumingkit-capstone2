const bcrypt = require("bcrypt")
const auth = require("../../auth")

const User = require("../../models/user/User");
//const Address = require("../../models/user/Address");

module.exports.register = (userData) =>
{
    const user = new User({
        // _id: new mongoose.Types.ObjectId(),
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        password: bcrypt.hashSync(userData.password, 10)
    });
    // const userAddress = new Address(userData.address);
    // userAddress.userId = user._id;

    return User.find({email: user.email}).then(result =>
    {
        if (result.length > 0) 
        {
            return {duplicateEmail: `Email already registered.`}
        } 
        else 
        {
            // userAddress.save().then(address => address);
            // user.address.push({addressId: userAddress._id});
            return user.save().then((user, error) =>
            {
                if(error)
                {
                    return false;
                }
                else
                {
                    return {access: auth.createAccessToken(user)};
                }
            });
        }
    })
}

module.exports.login = (userLogin) =>
{
    return User.findOne({email: userLogin.email}).then(result =>
    {
        if (result === null) 
        {
            return false;
        } 
        else 
        {
            const isPasswordCorrect = bcrypt.compareSync(userLogin.password, result.password);
            if (isPasswordCorrect) 
            {
                return {access: auth.createAccessToken(result)}
            } 
            else 
            {
                return false;
            }
        }
    })
}

module.exports.getProfile = (userId) =>
{
    const hideProps = {
        password: 0,
        registeredOn: 0
    }
    return User.findById(userId, hideProps).then(result =>
    {
        if (result === null) 
        {
            return false;
        } 
        else 
        {
            return result;
        }
    })
}

module.exports.updateProfile = (userId, userData) =>
{
    if (Object.hasOwn(userData, "isAdmin")) 
    {
        return false;
    } 
    else 
    {
        return User.findByIdAndUpdate(userId, userData).then(result => {
            return true;
        });
    }
}

module.exports.changePassword = (userId, password) =>
{
    const newPassword = bcrypt.hashSync(password.password, 10)


    return User.findByIdAndUpdate(userId, {password: newPassword}).then((result, error) => {
        if (error) {
            return false;
        }
        else {
            return true;
        }
    });
}