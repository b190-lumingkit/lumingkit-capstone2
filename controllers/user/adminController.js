const User = require("../../models/user/User");

module.exports.getAllUsers = () =>
{
    return User.find().then(result => result);
}

module.exports.promoteUser = (userId, status) =>
{
    return User.findByIdAndUpdate(userId, {isAdmin: status}).then(result => 
    {
        if (result) {
            return true;
        }
    });
}

module.exports.demoteUser = (userId) =>
{
    return User.findByIdAndUpdate(userId, {isAdmin: false}).then(result => 
    {
        if (result) {
            return true;
        }
    });
}