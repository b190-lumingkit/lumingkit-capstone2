const Product = require("../../models/product/Product");
const Ingredient = require("../../models/product/Ingredient");

// combine productData.ingredients and db.ingredients while retaining specific properties
async function mergeData(productData)
{
    let ingredNames = productData.ingredients.map(ingred => ingred.name);
    let ingredients = await Ingredient.find({"name": {"$in": ingredNames}}, {_id: 0, name: 1, pricePerServing: 1});

    const mergeData = ingredients.map(a => {
        let found = productData.ingredients.find(b => a.name === b.name);
        // found.serving = (found.serving === undefined) ? 1 : found.serving;
        found.serving = 1
        let c = { name: a.name, pricePerServing: a.pricePerServing, serving: found.serving}
        return c;
    })

    return mergeData;
}

function totalPrice(ingredList, productData)
{
    let totalPrice = 0;
    ingredList.forEach(item => totalPrice += item.pricePerServing * item.serving);

    // transfer pckgCost to frontend
    let pckgCost = 0;
    switch(productData.size.toLowerCase()) {
        case 'dako':
            pckgCost += 15
            break;
        case 'sakto':
            pckgCost += 10
            break;
        default:
            pckgCost += 5
            break;
    }

    return totalPrice + pckgCost;
}

module.exports.createProduct = async (productData) =>
{
    let ingredList = await mergeData(productData);
    
    let total = totalPrice(ingredList, productData)

    const product = new Product({
        name: productData.name,
        description: productData.description,
        ingredients: ingredList,
        size: productData.size,
        price: total
    })

    return Product.find({name: product.name}).then(result =>
    {
        if (result.length > 0) 
        {
            return false
        } 
        else 
        {
            return product.save().then(product => product);
        }
    })
}

module.exports.getAllProducts = () =>
{
    return Product.find().then(products => products);
}

module.exports.getAvailableProducts = () =>
{
    return Product.find({isAvailable: true}).then(products => products);
}

module.exports.getProduct = (productId) =>
{
    return Product.findById(productId).then(product => product); 
}



module.exports.updateProduct = (productId, productData) =>
{
    return Product.findByIdAndUpdate(productId, productData).then((product, error) => 
    {
        if (product)
        {
            return true;
        }
        else
        {
            return false;
        }
    });
}

module.exports.isProductAvailable = (productId, status) =>
{
    return Product.findByIdAndUpdate(productId, {isAvailable: status}).then((product, error) => 
    {
        if (product)
        {
            return true;
        }
        else
        {
            return false;
        }
    })
}

module.exports.removeProduct = (productId) =>
{
    return Product.findByIdAndUpdate(productId, {isAvailable: false}).then(product => `Product removed.`);
}

module.exports.brewYourCoffee = async (productData) =>
{
    let ingredList = await mergeData(productData);
    let total = totalPrice(ingredList, productData)

    const product = new Product({
        name: productData.name,
        description: productData.description,
        ingredients: ingredList,
        size: productData.size,
        price: total
    })

    return Product.find({name: product.name}).then(result =>
    {
        if (result.length > 0) 
        {
            return false;
        } 
        else 
        {
            return product.save().then(product => product);
        }
    })
}