const Order = require("../../models/product/Order");
const Address = require("../../models/user/Address");
const Product = require("../../models/product/Product");


module.exports.createOrder = async (userId, orderData) =>
{
    let totalPrice = 0;
    let products = [];

    for (const product of orderData.products) {
        const result = await Product.findOne({ name : product }, { _id: 0, name: 1, price: 1, size: 1 });
    
        products.push(result);
    }

    let address = await Address.findOne({userId: userId, _id: orderData.address}, {_id: 0, addedOn: 0, userId: 0, __v: 0});

    if (products.length > 0) 
    {
        products.forEach(product => totalPrice += product.price);
        const order = new Order({
            userId: userId,
            products: products,
            totalPrice: totalPrice,
            address: address,
            paymentMethod: orderData.paymentMethod
        });
    
        const result = await order.save();
        return result;
    } 
    else 
    {
        return false;
    } 
}

module.exports.getOrder = (orderId) =>
{
    return Order.findById(orderId).then((result, error) => {
        if (result) {
            return result;
        }
        else {
            return false;
        }
    })
}

module.exports.getAllOrders = () =>
{
    return Order.find().then(result => result);
}

module.exports.completeOrder = (orderId, status) =>
{
    return Order.findByIdAndUpdate(orderId, {status: status}).then(result => {
        if (result) {
            return true;
        }
    });
}

module.exports.cancelOrder = (orderId) =>
{
    return Order.findByIdAndUpdate(orderId, {status: "cancelled"}).then(result => `Order cancelled`);
}

module.exports.getUserOrders = (userId) =>
{
    return Order.find({userId: userId}).then(result => result);
}