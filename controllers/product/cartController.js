const Product = require("../../models/product/Product");
const Cart = require("../../models/product/Cart");


module.exports.getCartProducts = (cart) =>
{
    return Product.find({"_id": {"$in": cart.products}}).then(products => products);
}

module.exports.getUserCart = (userId) =>
{
    return Cart.findOne({userId: userId}).then(cart => {
        if (cart !== null)
        {
            return cart;
        } 
        else 
        {
            return false;
        }
    });
}

module.exports.addToCart = async (userId, userCart) =>
{
    const cart = await Cart.findOne({ userId: userId });
 
    if (cart !== null) 
    {
        const updatedCart = [...cart.products, ...userCart.products];
        cart.products = [...updatedCart];
        
        const result = await cart.save();
        return result;
    }
    else
    {
        const newCart = new Cart({
            userId: userId,
            products: userCart.products
        })

        const result = await newCart.save();
        return result;
    }
    
}

module.exports.updateCart = (userId, userCart) =>
{
    return Cart.findOneAndUpdate({userId: userId}, {products: userCart.products}).then((result, error) => {
        if (result) {
            return true;
        } 
        else {
            return false;
        }
    })
}