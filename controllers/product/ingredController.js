const Ingredient = require("../../models/product/Ingredient");


module.exports.getAllIngredients = () =>
{
    return Ingredient.find().gt("stocks", 0).then(result => result);
}

module.exports.addIngredient = (ingredData) =>
{
    const newIngred = new Ingredient({
        name: ingredData.name,
        category: ingredData.category,
        stocks: ingredData.stocks,
        pricePerServing: ingredData.price
    })

    return Ingredient.find({name: newIngred.name}).then(result =>
    {
        if (result.length > 0) 
        {
            return `Ingredient already exists.`
        } 
        else 
        {
            return newIngred.save().then(result => result);
        }
    })
}

module.exports.updateIngredient = (ingredId, ingredData) =>
{
    return Ingredient.findByIdAndUpdate(ingredId, ingredData).then(result => result);
}