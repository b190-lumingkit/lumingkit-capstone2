const Product = require("../../models/product/Product");
const Review = require("../../models/product/Review")

module.exports.addReview = async (userId, reviewData) =>
{
    let success = [];

    const products = await Product.find({"name": {"$in": reviewData.products}});
    const productIds = products.map(product => {
        return product._id.toString()
    })

    for (const product of productIds) {
        const review = new Review({
            userId: userId,
            productId: product,
            rating: reviewData.rating,
            description: reviewData.description
        })

        const reviewResult = await review.save();
        
        const productReviews = await Review.find({productId: product});
        let ratings = productReviews.map(review => review.rating);
        let avg = ratings.reduce((a, b) => a + b) / ratings.length;
 
        const result = await Product.findByIdAndUpdate(product, {rating: avg});
        
        if (result.id !== undefined) {
            success.push(true)
        }
    }

    return success;
}

module.exports.getProductReviews = (productId) =>
{
    return Review.find({productId: productId}).then((result, error) =>
    {
        if (error) {
            return false;
        }
        else {
            return true;
        }
    })
}